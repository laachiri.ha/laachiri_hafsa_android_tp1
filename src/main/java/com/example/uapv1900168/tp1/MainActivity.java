package com.example.uapv1900168.tp1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {


    ListView listview ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listview = (ListView) findViewById(R.id.listview);

        CountryList countryList = new CountryList();

        ArrayAdapter countryListAdapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1, countryList.getNameArray());
        listview.setAdapter(countryListAdapter);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String countryName = ((TextView) view).getText().toString();
                Intent countryActivity = new Intent(getBaseContext(),CountryActivity.class);
                countryActivity.putExtra("COUNTRY_NAME",countryName);
                startActivity(countryActivity);

                finish();
            }
        });

    }


}
