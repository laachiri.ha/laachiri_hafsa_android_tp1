package com.example.uapv1900168.tp1;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class CountryAdapter extends BaseAdapter {

    private Context context;
    private String[] countryList ;
    CountryList cl = new CountryList();
    private LayoutInflater inflater ;


    public CountryAdapter(Context context , String[] countryList){
        this.context = context;
        this.countryList = cl.getNameArray() ;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return cl.countryListsize();
    }

    @Override
    public Country getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        view = inflater.inflate(R.layout.adapter_item , null);
        //Country currentItem = getItem(i);
        //String itemName = currentItem.getmName();
        //TextView itemNameView = view.findViewById(R.id.item_name);
        //itemNameView.setText(itemName);


        return view;
    }
}
