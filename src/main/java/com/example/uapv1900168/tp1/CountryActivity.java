package com.example.uapv1900168.tp1;

import android.content.Context;
import android.support.design.widget.TextInputEditText;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.content.Intent;


public class CountryActivity extends AppCompatActivity {

    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country2);

        String countryName = getIntent().getStringExtra("COUNTRY_NAME");

        final Country country = CountryList.getCountry(countryName) ;

        TextView itemTitle = (TextView) findViewById(R.id.title);
        itemTitle.setText(countryName);

        final TextInputEditText capitalInput = (TextInputEditText) findViewById(R.id.capitalInput);
        capitalInput.setText(country.getmCapital());

        final TextInputEditText langueInput = (TextInputEditText) findViewById(R.id.langueInput);
        langueInput.setText(country.getmLanguage());

        final TextInputEditText monnaieInput = (TextInputEditText) findViewById(R.id.monnaieInput);
        monnaieInput.setText(country.getmCurrency());

        final TextInputEditText populationInput = (TextInputEditText) findViewById(R.id.populationInput);
        populationInput.setText(Integer.toString(country.getmPopulation()));

        final TextInputEditText superficieInput = (TextInputEditText) findViewById(R.id.superficieInput);
        superficieInput.setText(Integer.toString(country.getmArea()));

        ImageView itemImage = (ImageView) findViewById(R.id.imageView);
        switch (countryName) {
            case "France":
                itemImage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.flag_of_france));
                break;
            case "Espagne":
                itemImage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.flag_of_spain));

                break;
            case "Japon":
                itemImage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.flag_of_japan));
                break;
            case "Allemagne":
                itemImage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.flag_of_germany));
                break;
            case "États-Unis":
                itemImage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.flag_of_the_united_states));
                break;
            case "Afrique du Sud":
                itemImage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.flag_of_south_africa));
                break;
        }

        Button sauvegarder = (Button) findViewById(R.id.button_sauvegarder);

        sauvegarder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                country.setmCapital(capitalInput.getText().toString());
                country.setmLanguage(langueInput.getText().toString());
                country.setmCurrency(monnaieInput.getText().toString());
                country.setmPopulation(Integer.parseInt(populationInput.getText().toString()));
                country.setmArea(Integer.parseInt(superficieInput.getText().toString()));
                Intent mainActivity = new Intent(getBaseContext(),MainActivity.class);
                startActivity(mainActivity);

            }
        });


    }
}
